import React from "react";
import { Helmet } from "react-helmet";

const MetaTags = (props) => {
  // Create a REACT_APP_PUBLIC_URL property in your env file which is your fully qualified domain e.g. https://www.mydomain.com
  const PUBLIC_URL = "http://acidintegrations.ca";
  const url = PUBLIC_URL + window.location.pathname;
  const imageUrl = PUBLIC_URL + props.image;

  return (
    <Helmet>
      <title>{props.title}</title>
      <meta name="description" content={props.description} />
      <meta name="author" content="ACID Integrations" />
      <meta
        name="keywords"
        content="Vue.js, Unity, Unreal Engine, Game Development, Server Architecture, Back-End Development, UX Designer, Product Designer, Data-Driven, Content Strategy, UX Strategy, Design Strategy, Information Architecture, Web Design, Front-End Web Development, Sketchapp, Invision"
      ></meta>
      <meta property="og:type" content={props.ogType} />
      <meta property="og:site_name" content="ACID Integrations" />
      <meta property="og:title" content={props.title} />
      <meta property="og:description" content={props.description} />
      <meta property="og:url" content={url} />
      <meta property="og:image" content={imageUrl} />
      <meta property="og:image:width" content="1023" />
      <meta property="og:image:height" content="1017" />
      <meta property="twitter:card" content="summary_large_image" />
      <meta property="twitter:site" content="@acidintegrations" />
      <meta property="twitter:creator" content="@acidintegrations" />
      <meta property="twitter:title" content={props.title} />
      <meta property="twitter:description" content={props.description} />
      <meta property="twitter:url" content={url} />
      <meta property="twitter:image" content={imageUrl} />
    </Helmet>
  );
};

MetaTags.defaultProps = {
  title: "",
  description: "",
  ogType: "website, marketing",
  image: "/images/icons/icon-opengraph.png", // point to your default Opengraph image
};

export { MetaTags };
