import React, { useEffect } from "react";
import { Route, withRouter } from "react-router-dom";
import "@babel/polyfill";
import AsyncComponent from "./AsyncComponent";
import ReactPixel from "react-facebook-pixel";
import "./css/bulma.min.css";
import "./css/global-min.css";

const loadHome = () => {
  return import("./containers/HomeContainer").then((module) => module.default);
};
const HomeContainer = AsyncComponent(loadHome);

const loadInteractiveHome = () => {
  return import("./containers/InteractiveHomeContainer").then(
    (module) => module.default
  );
};
const InteractiveHomeContainer = AsyncComponent(loadInteractiveHome);

function App(props) {
  useEffect(() => {
    const advancedMatching = {}; // optional, more info: https://developers.facebook.com/docs/facebook-pixel/advanced/advanced-matching
    const options = {
      autoConfig: true, // set pixel's autoConfig. More info: https://developers.facebook.com/docs/facebook-pixel/advanced/
      debug: false, // enable logs
    };
    ReactPixel.init("1064442750668017", advancedMatching, options);
    loadHome();
  }, []);

  return (
    <div>
      <Route
        exact
        path="/"
        render={({ history, match }) => (
          <InteractiveHomeContainer history={history} />
        )}
      />
    </div>
  );
}

export default withRouter(App);