import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import App from "./App";

const rootElm = document.getElementById("root");
if (rootElm.hasChildNodes()) {
  ReactDOM.hydrate(
    <BrowserRouter>
      <App />
    </BrowserRouter>,
    rootElm
  );
} else {
  ReactDOM.render(
    <BrowserRouter>
      <App />
    </BrowserRouter>,
    rootElm
  );
}
// sends a new version of our app to ReactDOM.render when
// the files change.
// Hot Module Replacement will make our lives much easier,
if (module.hot) {
  module.hot.accept(["./App"], () => {
    const NextApp = require("./App").default;
    ReactDOM.render(
      <BrowserRouter>
        <NextApp />
      </BrowserRouter>,
      document.getElementById("root")
    );
  });
}
