import React, { Component } from "react";

// asyncComponent is a function that takes an import statement as
// an argument, which we call getComponent
// [228] The key for AsyncComponent will be the componentWillMount life cycle method.
export default function asyncComponent(getComponent) {
  return class AsyncComponent extends Component {
    state = { Component: null };
    componentWillMount() {
      if (!this.state.Component) {
        getComponent().then((Component) => {
          this.setState({ Component });
        });
      }
    }
    render() {
      const { Component } = this.state;
      if (Component) {
        // ...this.props is the JavaScript spread operator.
        // it basically means take the this.props object and copy
        // all its keys and values onto the props of Component .
        return <Component {...this.props} />;
      }
      return null;
    }
  };
}
