import React, { useState, useEffect } from "react";
import { MetaTags } from "../MetaTags";
import { NativeSelect } from "@material-ui/core";
import toast from "light-toast";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import ReactPixel from "react-facebook-pixel";

function HomeContainer(props) {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [company, setCompany] = useState("");
  const [phone, setPhone] = useState("");
  const [message, setMessage] = useState("");
  const [selectedOption, setSelectedOption] = useState("");
  const [sendButtonText, setSendButton] = useState("Send");

  const [value, setValue] = useState(0);

  const onChange = (value) => {
    // console.log(value);
    setValue(value);
  };

  const [width, setWidth] = useState(window.innerWidth);
  const [height, setHeight] = useState(window.innerHeight);
  const updateWH = () => {
    setWidth(window.innerWidth);
    setHeight(window.innerHeight);
  };

  useEffect(() => {
    window.addEventListener("resize", updateWH);

    var navbarBurgers = Array.prototype.slice.call(
      document.querySelectorAll(".navbar-burger"),
      0
    );

    // Check if there are any nav burgers
    if (navbarBurgers.length > 0) {
      // Add a click event on each of them
      navbarBurgers.forEach(function (el) {
        el.addEventListener("click", function () {
          // Get the target from the "data-target" attribute
          var target = el.dataset.target;
          var target = document.getElementById(target);

          // Toggle the class on both the "navbar-burger" and the "navbar-menu"
          el.classList.toggle("is-active");
          target.classList.toggle("is-active");
        });
      });
    }

    // const advancedMatching = {}; // optional, more info: https://developers.facebook.com/docs/facebook-pixel/advanced/advanced-matching
    // const options = {
    //   autoConfig: true, // set pixel's autoConfig. More info: https://developers.facebook.com/docs/facebook-pixel/advanced/
    //   debug: false, // enable logs
    // };
    // ReactPixel.init("1064442750668017", advancedMatching, options);
    // ReactPixel.revokeConsent();
    ReactPixel.pageView(); // For tracking page view
    return () => window.removeEventListener("resize", updateWH);
  }, []);

  const encode = (data) => {
    return Object.keys(data)
      .map(
        (key) => encodeURIComponent(key) + "=" + encodeURIComponent(data[key])
      )
      .join("&");
  };

  const handleSubmit = () => {
    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({
        "form-name": "contact-softwaredev",
        selectedOption,
        message,
        name,
        email,
        phone,
        company,
      }),
    })
      .then(() => {
        setSendButton("Sent");
        toast.success(
          "Thank you for completing the form, we will get in touch as soon as possible",
          5000,
          null
        );
        ReactPixel.trackCustom("Form-sent", {
          form: "sent-contact-software-dev",
        });
        // function waitForFbq(callback) {
        //   if (typeof fbq !== "undefined") {
        //     callback();
        //   } else {
        //     setTimeout(function () {
        //       waitForFbq(callback);
        //     }, 100);
        //   }
        // }
        // waitForFbq(function () {
        //   fbq("trackCustom", "Form-sent", {
        //     form: "sent-contact-software-dev",
        //   });
        // });
      })
      .catch((error) => toast.fail(error, 3000, null));

    e.preventDefault();
  };

  return (
    <div>
      <MetaTags
        title="ACID Integrations - Software Development"
        description="ACID Integrations is an End-to-End Digital Design and Development problem solver."
        image={"/assets/logo/logo-AI-6.png"}
      />
      <header>
        <section className="hero">
          <div className="hero-head">
            <nav
              className="navbar is-fixed-top"
              style={{ backgroundColor: "#38d430" }}
            >
              <div className="navbar-brand">
                <a className="navbar-item logo" href="/"></a>
                <div
                  className="navbar-burger burger"
                  data-target="navbarMenu"
                  aria-label="menu"
                  aria-expanded="false"
                >
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                </div>
              </div>
              <div id="navbarMenu" className="navbar-menu is-pulled-right">
                <div className="navbar-end">
                  <span
                    className="navbar-item"
                    style={{ backgroundColor: "#38d430" }}
                  >
                    <a
                      className="button brand-green"
                      href="#about"
                      data-target="about"
                    >
                      <span>About Us</span>
                    </a>
                  </span>
                  <span className="navbar-item">
                    <a
                      className="button brand-green"
                      href="#techstack"
                      data-target="techstack"
                    >
                      <span>Techstack</span>
                    </a>
                  </span>
                  <span className="navbar-item">
                    <a
                      className="button brand-green"
                      href="#contact"
                      data-target="contact"
                    >
                      <span>Contact</span>
                    </a>
                  </span>
                </div>
              </div>
            </nav>
          </div>
          <div className="hero-body ">
            {width >= 550 ? (
              <div className="container-header-slider">
                <Carousel
                  showThumbs={false}
                  onChange={onChange}
                  showStatus={false}
                  infiniteLoop={true}
                  swipeable={true}
                  autoPlay={true}
                  interval={10000}
                  transitionTime={750}
                  emulateTouch={true}
                  useKeyboardArrows={true}
                >
                  <div>
                    <img
                      src="/assets/images/korie-1.png"
                      className="image-header"
                      alt="header-logo"
                    />
                  </div>
                  <div>
                    <img
                      src="/assets/images/elevenyv-2 (1).png"
                      className="image-header"
                      alt="header-logo"
                    />
                  </div>
                  <div>
                    <img
                      src="/assets/images/cliniq-3.png"
                      className="image-header"
                      alt="header-logo"
                    />
                  </div>
                  <div>
                    <img
                      src="/assets/images/korie-3.png"
                      className="image-header"
                      alt="header-logo"
                    />
                  </div>
                  <div>
                    <img
                      src="/assets/images/Group 33.png"
                      className="image-header img3int"
                      alt="header-logo"
                    />
                  </div>
                </Carousel>
              </div>
            ) : (
              <div className="container-header-slider">
                <Carousel
                  showThumbs={false}
                  showArrows={false}
                  onChange={onChange}
                  showStatus={false}
                  infiniteLoop={true}
                  swipeable={true}
                  autoPlay={true}
                  interval={10000}
                  transitionTime={750}
                  emulateTouch={true}
                  useKeyboardArrows={true}
                >
                  <div>
                    <img
                      src="/assets/images/mobile1/korie-1.png"
                      className="image-header"
                      alt="header-logo"
                    />
                  </div>
                  <div>
                    <img
                      src="/assets/images/mobile1/elevenyv-2 (1).png"
                      className="image-header"
                      alt="header-logo"
                    />
                  </div>
                  <div>
                    <img
                      src="/assets/images/mobile1/cliniq-3.png"
                      className="image-header"
                      alt="header-logo"
                    />
                  </div>
                  <div>
                    <img
                      src="/assets/images/mobile1/Group 62.png"
                      className="image-header img3int"
                      alt="header-logo"
                    />
                  </div>
                </Carousel>
              </div>
            )}

          
            {value !== 0 ? null : width <= 400 ? (
              <div className="container-header">
                <div className="column is-6 is-offset-3 mt-5 is-mobile single-spaced">
                  <h1 className="title-head is-green">SOFTWARE</h1>
                  <h1 className="title-head is-white">DEVELOPMENT</h1>
                </div>
              </div>
            ) : (
              <div className="container-header">
                <h1 className="title-head is-green">SOFTWARE</h1>
                <h1 className="title-head is-white">DEVELOPMENT</h1>
              </div>
            )}

          </div>
        </section>
      </header>
      <main>
        <section className="section" id="about">
          <div className="container body">
            <h2
              className="title is-3 is-green is-bold"
              style={{ paddingBottom: "10px" }}
            >
              About us
            </h2>
            <div className="columns ">
              <div className="column is-three-fifths is-centered">
                <p className="subtitle info is-5">
                  We are an international company at the forefront of virtual
                  and visual technology. If you need forward-thinking software,
                  get in touch! We can help innovate and digitalize your
                  services, providing full end-to-end custom projects: from
                  strategy and design to full-stack web and mobile technology.
                </p>
              </div>
              <div
                className="column is-centered has-text-centered"
              >
                <img src="/assets/images/cliniq-1.jpg" />
              </div>
            </div>
          </div>
        </section>
        <section className="section " id="techstack">
          <div className="container body">
            <div className="columns is-mobile is-centered">
              <div className="column" style={{ paddingTop: 0 }}>
                <h2
                  className="title is-3 is-green is-bold"
                  style={{ paddingBottom: "10px" }}
                >
                  Our techstack
                </h2>
                {width >= 550 ? (
                  <div className="flex-images">
                    <div id="a" className="center-column">
                      <img src="/assets/images/Techstack/Group 137.png" />
                    </div>
                    <div id="b" className="center-column">
                      <img src="/assets/images/Techstack/Group 1.png" />
                    </div>
                    <div id="c" className="center-column">
                      <img src="/assets/images/Techstack/Group 129.png" />
                    </div>
                    <div id="d" className="center-column">
                      <img src="/assets/images/Techstack/Group 138.png" />
                    </div>
                    <div id="k" className="center-column">
                      <img src="/assets/images/Techstack/Android App.png" />
                    </div>
                    <div id="e" className="center-column">
                      <img src="/assets/images/Techstack/Group 139.png" />
                    </div>
                    <div id="f" className="center-column">
                      <img src="/assets/images/Techstack/Group 140.png" />
                    </div>
                    <div id="g" className="center-column">
                      <img src="/assets/images/Techstack/Group 130.png" />
                    </div>
                    <div id="h" className="center-column">
                      <img src="/assets/images/Techstack/Group 131.png" />
                    </div>
                    <div id="i" className="center-column">
                      <img src="/assets/images/Techstack/Server Development.png" />
                    </div>
                    <div id="j" className="center-column">
                      <img src="/assets/images/Techstack/Group 142.png" />
                    </div>
                  </div>
                ) : (
                  <div className="flex-images">
                    <div id="a" className="center-column">
                      <img src="/assets/images/Techstack-min/Group 137.png" />
                    </div>
                    <div id="b" className="center-column">
                      <img src="/assets/images/Techstack-min/Group 1.png" />
                    </div>
                    <div id="c" className="center-column">
                      <img src="/assets/images/Techstack-min/Group 129.png" />
                    </div>
                    <div id="d" className="center-column">
                      <img src="/assets/images/Techstack-min/Group 138.png" />
                    </div>
                    <div id="k" className="center-column">
                      <img src="/assets/images/Techstack-min/Android App.png" />
                    </div>
                    <div id="e" className="center-column">
                      <img src="/assets/images/Techstack-min/Group 139.png" />
                    </div>
                    <div id="f" className="center-column">
                      <img src="/assets/images/Techstack-min/Group 140.png" />
                    </div>
                    <div id="g" className="center-column">
                      <img src="/assets/images/Techstack-min/Group 130.png" />
                    </div>
                    <div id="h" className="center-column">
                      <img src="/assets/images/Techstack-min/Group 131.png" />
                    </div>
                    <div id="i" className="center-column">
                      <img src="/assets/images/Techstack-min/Server Development.png" />
                    </div>
                    <div id="j" className="center-column">
                      <img src="/assets/images/Techstack-min/Group 142.png" />
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </section>
        <section className="section" id="contact">
          <div className="container body">
            <div className="columns is-mobile is-centered">
              <div className="column" style={{ paddingTop: "2rem" }}>
                <h2
                  className="title is-3 is-green is-bold"
                  style={{ paddingBottom: "10px" }}
                >
                  Let's chat!
                </h2>
                <p
                  className="subtitle info is-5"
                  style={{ padding: "15px 0", fontSize: "24px" }}
                >
                  We're always interested to hear about new challenges and
                  opportunities.
                  <br /> Write us and we will get in touch as soon as possible.
                </p>
              </div>
            </div>
          </div>
          <div className="container body grey-box">
            <form
              onSubmit={handleSubmit}
              // name="contact-softwaredev"
              // method="POST"
              // data-netlify="true"
              action="/"
            >
             
              <label htmlFor="help-options" className="box-gray">
                How can we help you?
              </label>
              {width >= 650 ? (
                <div className="radio-container">
                  <div className="input-label">
                    <input
                      type="radio"
                      id="project"
                      name="help-options"
                      value="project-inquiry"
                      onChange={(e) => setSelectedOption(e.target.value)}
                      checked={selectedOption === "project-inquiry"}
                    />
                    <label htmlFor="project">Project Inquiry</label>
                  </div>
                  <div className="input-label">
                    <input
                      type="radio"
                      id="collab"
                      name="help-options"
                      value="collaboration-inquiry"
                      onChange={(e) => setSelectedOption(e.target.value)}
                      checked={selectedOption === "collaboration-inquiry"}
                    />
                    <label htmlFor="collab">Collaboration Inquiry</label>
                  </div>
                  <div className="input-label">
                    <input
                      type="radio"
                      id="career"
                      name="help-options"
                      value="career-inquiry"
                      onChange={(e) => setSelectedOption(e.target.value)}
                      checked={selectedOption === "career-inquiry"}
                    />
                    <label htmlFor="career">Career Inquiry</label>
                  </div>
                  <div className="input-label">
                    <input
                      type="radio"
                      id="other"
                      name="help-options"
                      value="other"
                      onChange={(e) => setSelectedOption(e.target.value)}
                      checked={selectedOption === "other"}
                    />
                    <label htmlFor="other">Other</label>
                  </div>
                </div>
              ) : (
                <div>
                  <NativeSelect
                    id="help-options"
                    name="help-options[]"
                    style={{
                      width: "100%",
                      outlineColor: "#707070",
                      fontSize: "25px",
                    }}
                    defaultValue="option1"
                    placeholder="Select an option"
                  >
                    <option value="project-inquiry">Project Inquiry</option>
                    <option value="collaboration-inquiry">
                      Collaboration Inquiry
                    </option>
                    <option value="career-inquiry">Career Inquiry</option>
                    <option value="other">Other</option>
                  </NativeSelect>
                </div>
              )}

              <label htmlFor="ideas-describe" className="box-gray light">
                Describe your ideas and needs in a few sentences
              </label>
              <textarea
                name="message"
                id="ideas-describe"
                rows="100"
                placeholder="Type here…."
                value={message}
                onChange={(e) => setMessage(e.target.value)}
                required
              />

              <div className="columns">
                <div className="column">
                  <div className="form-input-label">
                    <label htmlFor="name" className="box-gray box">
                      Name
                    </label>
                    <input
                      className="box"
                      type="text"
                      name="name"
                      id="name"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                      placeholder="John"
                      required
                    />
                  </div>
                  <div className="form-input-label">
                    <label htmlFor="phone" className="box-gray box">
                      Phone number (optional)
                    </label>
                    <input
                      className="box"
                      type="text"
                      name="phone"
                      id="phone"
                      value={phone}
                      onChange={(e) => setPhone(e.target.value)}
                      placeholder="888-888-8888"
                    />
                  </div>
                </div>
                <div className="column">
                  <div className="form-input-label">
                    <label htmlFor="email" className="box-gray box">
                      Email
                    </label>
                    <input
                      className="box"
                      type="email"
                      name="email"
                      id="email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      placeholder="John@example.com"
                      required
                    />
                  </div>
                  <div className="form-input-label">
                    <label htmlFor="company" className="box-gray box">
                      Company (optional)
                    </label>
                    <input
                      className="box"
                      type="text"
                      name="company"
                      id="company"
                      value={company}
                      onChange={(e) => setCompany(e.target.value)}
                      placeholder="Example Inc."
                    />
                  </div>
                </div>
              </div>
              <div hidden data-netlify-recaptcha="true"></div>
              <div className="button-form">
                <button type="submit" className="button-send">
                  {sendButtonText}
                </button>
              </div>
            </form>
          </div>
        </section>
        {/* <button className="consent-button" onClick={ReactPixel.grantConsent}>
          Accept cookies
        </button> */}
      </main>
      <footer className="hero-foot is-fullheight">
        <div className="columns is-multiline">
          <div className="column is-one-third">
            <h4 className="foot-title">Toronto Office</h4>
            <p className="foot-info">
              1585 Britannia Rd E #B3
              <br />
              Mississauga, ON L4W 2M4
              <br />
              Canada
              <br />
              <br />
              <a
                className="foot-link"
                href="https://www.google.com/maps/place/1585+Britannia+Rd+E,+Mississauga,+ON+L4W+2M4/@43.6579774,-79.6488764,17z/data=!3m1!4b1!4m5!3m4!1s0x882b38d3cc11a2f7:0x579f2373452d5dd8!8m2!3d43.6579774!4d-79.6466877"
                target="_blank"
              >
                View Map
              </a>
            </p>
          </div>

          <div className="column is-one-third">
            <h4 className="foot-title">General Inquiries</h4>
            <p className="foot-info">
              info@acidintegrations.com
              <br />
              Tel: 1-647-930-9305
              <br />
              Tel2: +351 932 080 132
            </p>
          </div>
          <div className="column is-one-third">
            <h4 className="foot-title">Follow Us</h4>
            <p className="foot-info">
              <a
                className="foot-link"
                href="https://www.facebook.com/acidintegrations"
                target="_blank"
              >
                Facebook
              </a>
              <br />
              <a
                className="foot-link"
                href="https://www.instagram.com/acidintegrations/"
                target="_blank"
              >
                Instagram
              </a>
              <br />
              <a
                className="foot-link"
                href="https://www.youtube.com/watch?v=MtG-cbhuesc"
                target="_blank"
              >
                Youtube
              </a>
              <br />
              <a
                className="foot-link"
                href="https://twitter.com/acidintegration"
                target="_blank"
              >
                Twitter
              </a>
              <br />
              <a
                className="foot-link"
                href="https://ca.linkedin.com/company/acid-integrations"
                target="_blank"
              >
                Linkedin
              </a>
            </p>
          </div>
          <p className="copy">
            &copy; 2019 ACID Integrations Ltd. All rights reserved.
          </p>
        </div>
      </footer>
    </div>
  );
}

export default HomeContainer;
